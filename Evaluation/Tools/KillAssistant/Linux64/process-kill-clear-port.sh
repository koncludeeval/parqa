#!/bin/bash
echo Trying termination for $1
pkill -SIGTERM -P $1
sleep 0.5


TRY=10
while [ $TRY -ne 0 ]
do
	echo "Checking port $2"
	PROCESS=`lsof -i:$2 | grep LISTEN | cut -d' ' -f 2`
	if [[ ! -z "$PROCESS" ]] 
	then
		echo "Port $2 is still used by process $PROCESS, trying termination"
		pkill -SIGTERM -P $PROCESS
		sleep 0.5
		TRY=$((TRY-1))
	else
		TRY=0
		echo "Port $2 is already free"
	fi	
	
done
