#!/bin/bash
memlimit=`expr 683687091200 / 1024`
echo Memory Limit: $memlimit
ulimit -Sv $memlimit
#BASHPID=$(sh -c 'echo $PPID')
./Evaluation/Reasoners/Konclude-SPARQL/Linux64/v0.7.0-1135/libs/Konclude $*
#trap "kill -- -$BASHPID" SIGINT SIGTERM EXIT
trap 'kill $(jobs -p)' SIGINT SIGTERM EXIT
