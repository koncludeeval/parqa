#!/bin/bash

echo "Note that PAGOdA is only available under an Academic Licence, see http://www.cs.ox.ac.uk/isg/tools/PAGOdA/PAGOdA_Academic_Licence.txt"

cd Evaluation/Reasoners/PAGOdA-SPARQL/Linux64/v2.0.0-prepare-jair/libs
wget http://www.cs.ox.ac.uk/isg/tools/PAGOdA/PAGOdA_Academic_Licence.txt
wget http://www.cs.ox.ac.uk/isg/tools/PAGOdA/2015/jair/PAGOdA.zip
unzip PAGOdA.zip
cp pagoda.jar pagoda_lib
