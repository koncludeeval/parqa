# Parallelised ABox Reasoning and Query Answering Evaluation Repository

This repository provides the evaluation framework and data for reproducing the results of the parallelised reasoning and query answering experiments, which are part of the following publication:

Andreas Steigmiller, Birte Glimm: Parallelised ABox Reasoning and Query Answering with Expressive Description Logics; In Proceedings of the 18th Extended Semantic Web Conference (ISWC), June 06-10, 2021, Heraklion, Greece.

An archived version of the evaluation framework and data (as used for the final paper) can be found at https://zenodo.org/record/4606566. This repository may contain some minor updates to improve the documentation and usability.


## Documentation

The accompanying [technical report](https://www.uni-ulm.de/fileadmin/website_uni_ulm/iui.inst.090/Publikationen/2021/StGl2021-PARQA-TR-ESWC.pdf "Technical Report") describes many details of the Parallelised ABox Reasoning and Query Answering (PARQA) approach and the integration in the reasoning system [Konclude](http://konclude.com "Konclude Website").


## Licences

This repository is a collection of tools, reasoners, and data (ontologies and queries), which are available as follows:
* Konclude (see http://konclude.con/ or https://github.com/konclude) available under the LGPLv3
* OWL BGP (see https://github.com/iliannakollia/owl-bgp) available under the LGPLv3
* Pellet (see https://github.com/stardog-union/pellet) available under an AGPLv3
* Reattore HTTP Server available under the GPL (see http://reattore.sourceforge.net/)
* Reactome ontology (https://reactome.org/) available under CC BY 4.0
* UniProt ontology (https://www.uniprot.org/) available under CC BY 4.0
* ChEMBL ontology (https://www.ebi.ac.uk/chembl/) available under CC BY-SA 3.0
* Several other ontologies that are available without specifying licences, e.g., UOBM, LUBM, etc (see files in corresponding folders).

Optionally, you can also integrate the following (reasoning) system (via the download script `download_and_install_pagoda_under_academic_licence.sh`):

* PAGOdA (see http://www.cs.ox.ac.uk/isg/tools/PAGOdA/) available under the academic licence

## Source Code

The source codes of all reasoners used in the evaluation is available in the `Sources.zip` file at [Zenodo](https://zenodo.org/record/4606566).

The parallelised reasoning and query answering approach via the individuals derivations cache is implemented in the included version of `Konclude` (v0.7.0-1135). Note that this version of `Konclude` is available under the version 3 of the Lesser General Public Licence (LGPLv3). For any use of the other reasoners, you also have to obey their licences, which are included in the corresponding folders.

For each reasoner, there exists a Linux bash script (`build.sh`/`build_release.sh`) that creates the binary for that reasoner (in a subfolder named release) as it is used by the evaluation framework. Note that pre-compiled versions of `Pellet`, `OWL BGP`, and `Konclude` for Linux 64bit are already included in this repository (see the `Evaluation/Reasoners/REASONER_NAME/REASONER_VERSION/libs` directories). For `PAGOdA`, only the pre-compiled adapter is included and you may use the script `download_and_install_pagoda_under_academic_licence.sh` to download and install `PAGOdA`. Note that for any use of `PAGOdA` you have to comply with the academic licence of `PAGOdA`. (The evaluation framework will try to evaluate `PAGOdA`, but will eventually skip to the next reasoner if `PAGOdA` is not available.) The other reasoners are available under the LGPLv3 (`OWL BGP`, `Konclude`) / the AGPLv3 (`Pellet`) and, hence, can freely be used.
For compiling the reasoners from scratch, the following tools/libraries are required:

* JDK (required for `PAGOdA` and `OWL BGP`), JDK 8 (required for `Pellet`)
* Maven (required for Pellet and `PAGOdA`)
* Ant (required for `OWL BGP`)
* Antlr (required for `Pellet`)
* Docker (required for `Konclude`)

Note that `Konclude` can also be compiled without Docker, but this usually leads to a more system dependent binary. For nevertheless compiling `Konclude` manually, you can follow the commands in the Dockerfiles and/or check the `Konclude` readme.

## Evaluation

This repository contains the evaluation framework with all queries and TBoxes for the evaluated ontologies. The ABoxes must be downloaded from the [PAGOdA Website](http://www.cs.ox.ac.uk/isg/tools/PAGOdA/2015/jair/ "PAGOdA Website"), which can be realised via the script `download-unpack-ontology-data.sh`. Note that the download script is also executed if the evaluation is started via the script `run-parallelization-query-answering-experiments.sh` or `run-all.sh`, but it checks whether the ABoxes are already present.

For the evaluation, we used `Konclude’s` integrated evaluator (i.e., the "Evaluator" binary in the `Evaluator` directory is also a binary of the reasoner `Konclude`). The evaluations can directly be executed on a Linux 64bit system by running the script `run-parallelization-query-answering-experiments.sh`. Note that the experiment takes at least 10 days. Note that the port 8880 must be free since it is used by the reasoners as their SPARQL HTTP endpoint. By default, only different versions of `Konclude` with varying numbers of threads are evaluated, but other reasoners (most notably PAGOdA) can be uncommented in `Evaluation/Programs/Querying-SPARQL-ParallelizationTests-Scaled001.xml` such that they are included in the evaluation (which requires an installed JRE). Note that it is not recommended to include `OWL BGP` or `Pellet` in the evaluation since they are not really able to handle these enourmous ontologies.

As an alternative to directly executing the evaluation, a Docker image can be created from the included Dockerfile (see script `create-evaluation-image.sh` in the `Docker` directory) that will run the experiment as soon as it is executed and, subsequently, it will start a webserver that exposes the results. If the image is built, then the evaluation container can be started as follows:

```
docker run -p 8888:8888 --rm koncludeeval/parqa
```
For saving the results and analyses, you may run the container with mounted directories as follows:
```
docker run -p 8888:8888 --rm -v /path/to/save/results/Analyses:/data/Evaluation/Analyses -v /path/to/save/results/Responses:/data/Evaluation/Responses koncludeeval/parqa
``` 

## Results

All reasoner responses are saved into subdirectories of the `Evaluation/Responses` folder, but the evaluator is analysing all responses in the end and summarises some results in the `Evaluation/Analyses` directory. 
(If the evaluation framework is executed as a Docker container, then these responses and analyses can be found in subfolders of the directories that are mounted for `/data/Evaluation/Responses` and `/data/Evaluation/Analyses`).
An overview over the summarised results can be found in the HTML pages `Evaluation/Analyses/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html`
and `Evaluation/Analyses/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html`, which should provide links to the most interesting charts and tables.

You may want to run the script `run-analyses-webserver.sh` to start a small web sever that exposes the `Evaluation/Analyses` directory on port [8888](http://localhost:8888 "http://localhost:8888") (which is automatically done for the Docker container of the evaluation framework after the experiments have completed) such that you can easily navigate through the summarised results.
The most interesting pages are probably as follows (assuming the web server has been started on the localhost):
* The [OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview](http://localhost:8888/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html) page in the `Querying-SPARQL-ParallelizationTests-Scaled001/` directory lists the most interesting charts/tables for the query answering experiment.
* The [complex-querying-accumulated-time-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/complex-querying-accumulated-time-table-vertical-chart.html) shows the accumulated query answering time for each reasoner (version) over all or a specific group of queries/ontologies. For example, [complex-querying-accumulated-time-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/ParallelizationTestQueries/SpecialSelections/AbsoprtionBasedQueryAnswering/PAGOdATestOntologies/chembl/sample-100/complex-querying-accumulated-time-table-vertical-chart.html) for chembl shows the accumulated query answering times for the chembl queries. Moreover, [time-mmm-separate-sorted-table-horizontal-chart](http://localhost:8888/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/ComplexQueryingTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/time-mmm-separate-sorted-table-horizontal-chart.html) in shows the query answering times (total processing time for the ResponseTimeComparison category) separately sorted for each reasoner (version), i.e., the smallest answering time (processing time) for each reasoner (version) is shown on the left and the largest answering time (processing time) is shown on the right.
* The [loading-accumulated-time-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-ParallelizationTests-Scaled001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/loading-accumulated-time-table-vertical-chart.html) shows the accumulated loading time for each reasoner (version) over all or a specific group of queries/ontologies, where the loading time includes the time to parse the ontology and to prepare it for query answering (i.e., the usual preprocessing steps such as absorption, consistency checking, classification). Note that each query is evaluated sparately, i.e., the loading times must be divided by the number of queries in order to get the actual preparation/preprocessing time. Unfortunately, the parsing times are not yet extracted, but they can easily be found in the stored reasoner responses. The precomputation time can be obtained by subtracting the parsing time form the loading time.
* The underlying data of most charts can also be download as CSV files (see links on the bottom of the pages).


All results are "cached" in files, i.e., if you stop and restart the evaluation, then it will continue where you stopped it. If you want to start the evaluation from scratch, then you have to delete the responses and analyses directories, e.g., by executing the `clean.sh` script (or by mounting new/different directories for the Docker container).

Our results (obtained from executing the experiments on a Dell PowerEdge R420 server with two Intel Xeon E5-2440 CPUs at 2.4 GHz and 144 GB RAM under a 64bit Ubuntu 16.04.5 LTS) are available at Zenodo (see Results.zip archive).
