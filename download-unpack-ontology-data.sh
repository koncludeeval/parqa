#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"                                                           
ROOT="$(realpath $DIR)"


echo "Checking, downloading, and unpacking ontology data"

cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/uobm
if [ ! -f data/uobm500.ttl ]; then	
	echo "Downloading and unpacking UOBM ontology data"
	rm uobm500.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/uobm/data/uobm500.zip
	unzip uobm500.zip -d data
fi

cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/lubm
if [ ! -f data/lubm800.ttl ]; then	
	echo "Downloading and unpacking LUBM ontology data"
	rm lubm800.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/lubm/data/lubm800.zip
	unzip lubm800.zip -d data
fi

cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/uniprot
if [ ! -f data/sample_100.nt ]; then	
	echo "Downloading and unpacking Uniprot (100%) ontology data"
	rm sample_100.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/uniprot/data/sample_100.zip
	unzip sample_100.zip -d data
fi


cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/uniprot
if [ ! -f data/sample_40.nt ]; then	
	echo "Downloading and unpacking Uniprot (40%) ontology data"
	rm sample_40.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/uniprot/data/sample_40.zip
	unzip sample_40.zip -d data
fi

cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/reactome
if [ ! -f data/sample_100.ttl ]; then	
	echo "Downloading and unpacking Reactome ontology data"
	rm sample_100.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/reactome/data/sample_100.zip
	unzip sample_100.zip -d data
fi

cd $ROOT
cd Evaluation/Ontologies/O-000/SpecialSelections/AbsorptionBasedQueryAnswering/PAGOdATestOntologies/chembl
if [ ! -f data/ChEMBL.ttl ]; then	
	echo "Downloading and unpacking ChEMBL ontology data"
	rm ChEMBL.zip
	wget --no-check-certificate https://krr-nas.cs.ox.ac.uk/2015/jair/PAGOdA/TestCases/chembl/data/ChEMBL.zip
	unzip ChEMBL.zip -d data
fi

cd $DIR

echo `pwd`