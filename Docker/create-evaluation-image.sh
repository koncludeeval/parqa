#!/usr/bin/env bash
ORIG=$(pwd)                                                                                                       
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"                                                           
ROOT="$(realpath $DIR/..)"
PROJ="$ROOT/"


cp -p "$DIR/Dockerfile" "$ROOT"

cd "$ROOT"


docker build . -t "koncludeeval/parqa:latest"

rm "$ROOT/Dockerfile"
cd "$DIR"


echo "Evaluation image created, executable as follows:"
echo "docker run -p 8888:8888 --rm koncludeeval/parqa"
echo "docker run -p 8888:8888 --rm -v /path/to/save/analyses/results:/data/Evaluation/Analyses -v /path/to/save/responses:/data/Evaluation/Responses koncludeeval/parqa"
echo

cd "$ORIG"
